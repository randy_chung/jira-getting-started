import SectionMessage from '@atlaskit/section-message';
import React, { useState } from 'react';

import { testDogeCall, getOpenIssues } from '../src/services/api-service';

export default function HelloWorld() {
  const [excitementLevel, setExcitementLevel] = React.useState<number>(0);

  const [dogeUrl, setDogeUrl] = useState('');

  const doRequest = () => {
    testDogeCall().then((imgSrc: string) => {
      setDogeUrl(imgSrc);
    });
  };

  const getIssues = () => {
    getOpenIssues();
  };

  return (
    <SectionMessage
      title={`Hello, world${excitementLevel ? new Array(excitementLevel).fill('!').join('') : '.'}`}
      actions={[
        {
          key: '1',
          href: 'https://atlassian.design/components/',
          text: 'Browse more components to add to your app'
        },
        {
          key: '2',
          onClick: () => setExcitementLevel(excitementLevel + 1),
          text: 'Get excited!'
        }
      ]}
    >
      <p>
        Congratulations! You have successfully created an Atlassian Connect app using the{' '}
        <a href={'https://bitbucket.org/atlassian/atlassian-connect-express'}>
          atlassian-connect-express
        </a>{' '}
        client library.
      </p>
      <p>
        <i>
          This file was generated from a <strong>Typescript</strong> React source file.
        </i>
      </p>

      <p>
        Test Axios HTTP request here:
        <button onClick={doRequest}>Do Request</button>
        <img src={dogeUrl}></img>
      </p>

      <p>
        Get some JIRA issues here:
        <button onClick={getIssues}>Get Open Issues</button>
      </p>
    </SectionMessage>
  );
}
