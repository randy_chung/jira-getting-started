/**
 *
 * This service provides functions that allow us to make calls to APIs via HTTP requests.
 *
 */

import axios from 'axios';

/**
 * Allows us to access the global `window.AP` object (the connect Javascript API) without TS
 * complaining.
 */
declare global {
  interface Window {
    AP: any;
  }
}

/**
 * Just a temporary test/POC function that does an HTTP request.
 */
export const testDogeCall = (): Promise<string> => {
  return axios
    .get('https://dog.ceo/api/breeds/image/random')
    .then((response: { data: { message: string } }) => response.data.message);
};

/**
 * Call the Atlassian API to get all open issues.
 *
 * Example API syntax:
 *      https://manchesterworks.atlassian.net
 *      /rest/api/3/search?fields=[summary]&jql=status=%22To%20Do%22%20OR%20status=%22Done%22
 */
export const getOpenIssues = () => {
  // TODO (Randy Chung - 2021-09-06): If we're getting all the JIRA data via the Connect JS API,
  // maybe we don't even need axios?
  window.AP.user.getCurrentUser(function (user: any) {
    console.log('The Atlassian Account ID is', user.atlassianAccountId);
  });
};
